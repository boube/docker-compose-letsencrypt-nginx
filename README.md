Setup 
==

* Run setup.sh 

``` ./setup.sh domain email ```

* Edit the  nginx/conf.d/default.conf  and serve with https whatever you want. 

Route53 
==

```docker run -it -v $PWD/nginx/letsencrypt:/etc/letsencrypt certbot/dns-route53  certonly --email $EMAIL --agree-tos  --no-eff-email  --dns-route53 -d $DOMAIN```
