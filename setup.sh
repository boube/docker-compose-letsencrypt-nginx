#!/bin/bash

[ $# -lt 2 ] && echo "Usage: $0 valid@email.com domain1.com [ -d domain2.com ] [ -d domainN.com ] " && exit 1 

email=${1} ; shift 

docker run \
   -v $PWD/nginx/letsencrypt:/etc/letsencrypt  \
   -p 80:80  \
   --name letsencrypt \
   --rm \
   -it \
   certbot/certbot \
   certonly \
     --standalone  \
     --email ${email} \
     --agree-tos \
     --no-eff-email \
     -d ${@}

sed "s/your_own_domain/${2}/g" -i nginx/conf.d/default.conf

